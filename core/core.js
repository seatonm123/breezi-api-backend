const Auth = require('./auth');
const User = require('./partneredUsers');
const Building = require('./building');
const Device = require('./device');

exports.runCore = function(jwt){
  return new Promise((resolve, reject)=>{
    Auth.verifyPartner(jwt)
      .then(partner=>{
        User.retrievePartneredUsers(partner)
          .then(users=>{
            Building.retrieveBuildings(users)
              .then(buildings=>{
                Device.retrieveDevices(buildings)
                  .then(devices=>{
                    resolve(devices);
                  }, deviceErr=>{
                    reject({code: deviceErr.code, msg: deviceErr.msg});
                  });
              }, buildingErr=>{
                reject({code: buildingErr.code, msg: buildingErr.msg});
              });
          }, usersErr=>{
            reject({code: usersErr.code, msg: usersErr.msg});
          });
      }, partnerErr=>{
        reject({code: partnerErr.code, msg: partnerErr.msg});
      })
  });
};
