const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});

const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

const DEVICE_TABLE = 'Devices';
const ENVIRONMENT_SUFFIX = '-matt';

exports.retrieveDevices = function(buildings){
  return new Promise((resolve, reject)=>{
    userIds = buildings.map(b => b.userId);
    Dynamo.scan({TableName: DEVICE_TABLE + ENVIRONMENT_SUFFIX}, (err, devices)=>{
      if (err) {
        reject({code: 400, msg: 'No devices found for users affiliated with this partner'});
      } else {
        devices.Items.map(d=>{
          buildings.map(b=>{
            b.building.airVents.map(v=>{
              if (v.airVentId === d.airVentAssignments[0].airVentId) {
                let vent = v;
                vent.device = d.deviceId;
              }
            });
          });
        });
        resolve(buildings);
      }
    });
  });
};
