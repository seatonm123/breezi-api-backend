exports.runFilters = function(userId, buildingId, ventId, data){
  return new Promise((resolve, reject)=>{
    if (userId === null && buildingId === null && ventId === null) {
      compileVents(data)
        .then(ventData=>{
          resolve(ventData)
        }, compileErr=>{
          reject({code: compileErr.code, msg: compileErr.msg});
        });
    } else {
      if (userId !== null) {
        getVentsByUser(userId, data)
          .then(userVents=>{
            compileVents(userVents)
              .then(userData=>{
                resolve(userData)
              }, compileErr=>{
                reject({code: compileErr.code, msg: compileErr.msg});
              });
          }, userErr=>{
            reject({code: userErr.code, msg: userErr.msg});
          });
      } else if (buildingId !== null) {
        getVentsByBuilding(buildingId, data)
          .then(buildingVents=>{
            compileVents(buildingVents)
              .then(buildingData=>{
                resolve(buildingData);
              }, compileErr=>{
                reject({code: compileErr.code, msg: compileErr.msg})
              });
          }, buildingErr=>{
            reject({code: buildingErr.code, msg: buildingErr.msg});
          });
      } else if (ventId !== null) {
        getVentById(ventId, data)
          .then(singleVent=>{
            if (singleVent.length < 1) {
              reject({code: 400, msg: 'No airVent found with requested id'});
            } else if (singleVent.length > 1) {
              reject({code: 500, msg: 'Conflicting airVent ids: more than one airVent with requested id found'});
            } else {
              resolve(singleVent);
            }
          }, singleErr=>{
            reject({code: singleErr.code, msg: singleErr.msg});
          });
      }
    }
  });
};

function compileVents(buildings){
  return new Promise((resolve, reject)=>{
    if (!buildings || buildings === undefined || buildings.length < 1) {
      reject({code: 400, msg: 'No buildings affiliated with this partner found'});
    } else {
      let returnBuildings = [];
      buildings.map(b=>{
        b.building.airVents.map(vent=>{
          returnBuildings.push({
            userId: b.userId,
            buildingId: b.building.buildingId,
            deviceId: vent.deviceId,
            airVent: {airVentId: vent.airVentId, airFilters: vent.airFilters.map(v => v)}
          });
        });
      });
      if (!returnBuildings || returnBuildings === undefined || returnBuildings.length < 1) {
        reject({code: 400, msg: 'No buildings affiliated with this partner found'});
      } else {
        resolve(returnBuildings);
      }
    }
  });
}

function getVentsByUser(userId, buildings){
  return new Promise((resolve, reject)=>{
    let filtered = buildings.filter(building => building.userId === userId);
    if (filtered.length < 0) {
      reject({code: 400, msg: 'No vents found for requested user'});
    } else {
      resolve(filtered);
    }
  });
}

function getVentsByBuilding(buildingId, buildings){
  return new Promise((resolve, reject)=>{
    let filtered = buildings.filter(building => building.building.buildingId === buildingId);
    if (filtered.length < 1) {
      reject({code: 400, msg: 'No vents found for requested building'});
    } else {
      resolve(filtered);
    }
  });
}

function getVentById(ventId, buildings){
  return new Promise((resolve, reject)=>{
    let returnVent = [];
    buildings.map(building=>{
      building.building.airVents.map(vent=>{
        if (vent.airVentId === ventId) {
          returnVent.push({
            userId: building.userId,
            buildingId: building.building.buildingId,
            airVent: vent
          });
        }
      });
    });
    if (returnVent.length < 1) {
      reject({code: 400, msg: 'No vent found with requested id'});
    } else if (returnVent.length > 1) {
      reject({code: 400, msg: 'Conflicting vent ids: more than one vent has this id ||' + JSON.stringify(returnVent)})
    } else {
      resolve(returnVent);
    }
  });
}
