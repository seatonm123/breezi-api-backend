const Core = require('./core/core');
const User = require('./user');

exports.handler = function(event, context, cb){
  const response = (code, msg)=>{
    if (msg) console.log(JSON.stringify(msg, null, 1));
    cb(null, {
      statusCode: code,
      body: msg ? JSON.stringify(msg) : '',
      headers: {
        "Content-Type": "application/json"
      }
    });
  };

  var accessToken = event.headers && event.headers.Authorization ?
    event.headers.Authorization : null;

  var userParam = event.queryStringParameters && event.queryStringParameters.user ?
    event.queryStringParameters.user : null;

  Core.runCore(accessToken)
    .then(coreData=>{
      if (event.httpMethod === "GET") {
        User.getUsersByParam(userParam, coreData)
          .then(usersByParam=>{
            response(200, usersByParam);
          }, paramErr=>{
            response(paramErr.code, paramErr.msg);
          });
      } else {
        response(405, 'HTTP Method not specified or not allowed');
      }
    }, coreErr=>{
      response(coreErr.code, coreErr.msg);
    });
};
