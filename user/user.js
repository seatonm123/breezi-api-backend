exports.getUsersByParam = function(userId, coreData){
  return new Promise((resolve, reject)=>{
    if (userId === null) {
      responseData = coreData;
      if (coreData && coreData.length > 0) {
        resolve(coreData);
      } else {
        reject({code: 400, msg: 'No affiliated users were found for this partner'});
      }
    }
    let userInQuestion = coreData.find(user => user.userId === userId);
    if (!userInQuestion) {
      reject({code: 400, msg: 'User with requested id not found'});
    } else {
      resolve([userInQuestion]);
    }
  });
};
