
exports.runFilters = function(userId, buildingId, data){
  return new Promise((resolve, reject)=>{
    if (userId === null && buildingId === null) {
      returnData = data;
      if (returnData.length > 0) {
        let returnArr = [];
        returnData.map(user=>{
          returnArr.push({userId: user.userId, buildingId: user.building.buildingId});
        });
        resolve(returnArr);
      } else {
        reject({code: 400, msg: 'Buildings for requested users affiliated with this partner not found'});
      }
    } else {
      if (userId !== null) {
        let byUser = data.filter(user => user.userId === userId);
        let userResponse = [];
        byUser.map(u=>{
          userResponse.push({
            userId: userId,
            buildingId: u.building.buildingId
          });
        });
        if (userResponse && userResponse !== undefined && userResponse.length > 0) {
          resolve(userResponse);
        } else {
          reject({code: 400, msg: 'No buildings belonging to requested user found'});
        }
      } else if (buildingId !== null) {
        let byBuilding = data.filter(bldg => bldg.building.buildingId === buildingId);
        let buildingResponse = [];
        byBuilding.map(b=>{
          buildingResponse.push({
            userId: b.userId,
            buildingId: buildingId,
            airVents: b.building.airVents.map(vent => vent.airVentId)
          });
        });
        if (buildingResponse && buildingResponse !== undefined && buildingResponse.length > 0) {
          resolve(buildingResponse);
        } else {
          reject({code: 400, msg: 'No building found with requested id'});
        }
      }
    }
  });
};
