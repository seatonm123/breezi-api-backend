const Core = require('./core/core');
const Filter = require('./filter');

exports.handler = function(event, context, cb){
  const response = (code, msg)=>{
    if (msg) console.log(JSON.stringify(msg, null, 1));
    cb(null, {
      statusCode: code,
      body: msg ? JSON.stringify(msg) : '',
      headers: {
        "Content-Type": "application/json"
      }
    });
  };

  let accessToken = event.headers && event.headers.Authorization ?
    event.headers.Authorization : null;

  let userParam = event.queryStringParameters && event.queryStringParameters.user ?
    event.queryStringParameters.user : null;

  let buildingParam = event.queryStringParameters && event.queryStringParameters.building ?
    event.queryStringParameters.building : null;

  Core.runCore(accessToken)
    .then(coreData=>{
      if (event.httpMethod === "GET") {
        Filter.runFilters(userParam, buildingParam, coreData)
          .then(filteredBuildings=>{
            response(200, filteredBuildings);
          }, filterErr=>{
            response(filterErr.code, filterErr.msg);
          });
      } else {
        response(303, 'HTTP method not supported. Please see the documentation in the Help section of the API client for details on modeling an HTTP request');
      }
    }, coreErr=>{
      response(coreErr.code, coreErr.msg);
    });

};
