exports.runQueries = function(from, to, val, data){
  return new Promise((resolve, reject)=>{
    runFrom(from, data)
      .then(fromMeas=>{
        runTo(to, fromMeas)
          .then(toMeas=>{
            runVal(val, toMeas)
              .then(valMeas=>{
                resolve(valMeas);
              }, valErr=>{
                reject({code: valErr.code, msg: valErr.msg});
              });
          }, toErr=>{
            reject({code: toErr.code, msg: toErr.msg});
          });
      }, fromErr=>{
        reject({code: fromErr.code, msg: fromErr.msg});
      });
  });
};

function runFrom(from, measurements) {
  return new Promise((resolve, reject)=>{
    if (from === null) {
      resolve(measurements);
    } else {
      let promises = measurements.map(meas => parseFrom(from, meas));
      Promise.all(promises)
        .then(fromResponse=>{
          resolve(fromResponse);
        }, parseErr=>{
          reject({code: 400, msg: 'No measurements found after timestamp: ' + from.toString()});
        });
    }
  });
}

function parseFrom(from, measurement){
  return new Promise((resolve, reject)=>{
    measurement.building.airVents.map(vent=>{
      vent.airFilters.map(filter=>{
        let filteredMeas = filter.measurements.filter(f => f.tms >= from);
        filter.measurements = filteredMeas;
      });
    });
    resolve(measurement);
  });
}

function runTo(to, measurements) {
  return new Promise((resolve, reject)=>{
    if (to === null) {
      resolve(measurements);
    } else {
      let promises = measurements.map(meas => parseTo(to, meas));
      Promise.all(promises)
        .then(toResponse=>{
          resolve(toResponse);
        }, parseErr=>{
          reject({code: 400, msg: 'No measurements found before timestamp: ' + to.toString()});
        });
    }
  });
}

function parseTo(to, measurement){
  return new Promise((resolve, reject)=>{
    measurement.building.airVents.map(vent=>{
      vent.airFilters.map(filter=>{
        filter.measurements = filter.measurements.filter(f => f.tms <= to);
      });
    });
    resolve(measurement);
  });
}

function runVal(val, measurements){
  return new Promise((resolve, reject)=>{
    if (val === null) {
      resolve(measurements);
    } else {
      let promises = measurements.map(meas => parseVal(val, meas));
      Promise.all(promises)
        .then(valMeas=>{
          resolve(valMeas);
        }, valErr=>{
          reject({code: 400, msg: 'Invalid or non-existent val query parameter'});
        });
    }
  });
}

function parseVal(val, measurement){
  return new Promise((resolve, reject)=>{
    measurement.building.airVents.map(vent=>{
      vent.airFilters.map(filter=>{
        let returnVals = [];
        filter.measurements.map(meas=>{
          let returnVal = {tms: meas.tms};
          returnVal[val] = meas[val];
          returnVals.push(returnVal);
        });
        filter.measurements = returnVals;
      });
    });
    resolve(measurement);
  });
}
