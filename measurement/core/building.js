const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});

const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

const BUILDING_TABLE = 'Buildings';
const ENVIRONMENT_SUFFIX = '-matt';

exports.retrieveBuildings = function(users){
  return new Promise((resolve, reject)=>{
    Dynamo.scan({TableName: BUILDING_TABLE + ENVIRONMENT_SUFFIX}, (err, buildings)=>{
      if (err) {
        reject({code: 400, msg: 'No buildings found'});
      } else {
        let buildingObjs = [];
        users.map(u=>{
          let userBldgs = buildings.Items.filter(b => b.userId === u.id);
          userBldgs.map(bldg=>{
            let bldgObj = {
              userId: u.id,
              building: {
                buildingId: bldg.id,
                airVents: []
              }
            };
            bldg.airVents.map(v=>{
              let vent = {
                airVentId: v.id,
                airFilters: v.airFilters.map(f => f.id)
              };
              bldgObj.building.airVents.push(vent);
            });
            buildingObjs.push(bldgObj);
          });
        });
        if (buildingObjs.length > 0) {
          resolve(buildingObjs);
        } else {
          reject({code: 400, msg: 'No building data was found for the users affiliated with this partner'});
        }
      }
    });
  });
};
