const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});

const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

const MEASUREMENT_TABLE = 'Measurements';
const ENVIRONMENT_SUFFIX = '-matt';

exports.getAllMeasurements = function(buildings){
  return new Promise((resolve, reject)=>{
    let promises = buildings.map(building=>runMeas(building));
    Promise.all(promises)
      .then(allMeas=>{
        resolve(allMeas);
      }, allErr=>{
        reject({code: 400, msg: 'No measurements found for this partner'});
      });
  });
};

function runMeas(building){
  return new Promise((resolve, reject)=>{
    let promises = []
    building.building.airVents.map(vent=>{promises.push(parseMeas(vent))});
    Promise.all(promises)
      .then(buildingMeas=>{
        let returnBuilding = {
          userId: building.userId,
          building: {
            buildingId: building.building.buildingId,
            airVents: buildingMeas
          }
        };
        resolve(returnBuilding);
      }, buildingErr=>{
        reject({code: 400, msg: 'No measurements found for this building'});
      });
  });
}

function parseMeas(vent){
  return new Promise((resolve, reject)=>{
    Dynamo.query({
      TableName: MEASUREMENT_TABLE + ENVIRONMENT_SUFFIX,
      KeyConditionExpression: 'did = :deviceId',
      ExpressionAttributeValues: {
        ':deviceId': vent.device
      }
    }, (err, measurements)=>{
      if (err) {
        reject({code: 400, msg: 'No measurements affiliated with this partner found'});
      } else {
        let returnVent = {
          airVentId: vent.airVentId,
          airFilters: []
        };
        vent.airFilters.map(filter=>{
          returnVent.airFilters.push({
            airFilterId: filter,
            device: vent.device,
            measurements: measurements.Items.filter(m => m.airFilterId === filter).map(meas=>{return flatten(meas)}).sort((a, b)=>{return a.tms - b.tms})
          });
        });
        resolve(returnVent);
      }
    });
  });
}

function flatten(meas){
  let returnObj = {};
  for (var prop in meas) {
    if (prop !== 'dat' && prop !== 'airFilterId') {
      returnObj[prop] = meas[prop];
    }
  }
  for (var datProp in meas.dat) {
    returnObj[datProp] = meas.dat[datProp][0];
  }
  return returnObj;
}
