exports.runFilters = function(user, building, vent, filter, device, data){
  return new Promise((resolve, reject)=>{
    if (user !== null) {
      filterByUser(user, data)
        .then(userResponse=>{
          resolve(userResponse);
        }, userErr=>{
          reject({code: userErr.code, msg: userErr.msg});
        });
    } else if (building !== null) {
      filterByBuilding(building, data)
        .then(buildingResponse=>{
          resolve(buildingResponse);
        }, buildingErr=>{
          reject({code: buildingErr.code, msg: buildingErr.msg});
        });
    } else if (vent !== null) {
      filterByVent(vent, data)
        .then(ventResponse=>{
          resolve(ventResponse);
        }, ventErr=>{
          reject({code: ventErr.code, msg: ventErr.msg});
        });
    } else if (filter !== null) {
      filterByFilter(filter, data)
        .then(filterResponse=>{
          resolve(filterResponse);
        }, filterErr=>{
          reject({code: filterErr.code, msg: filterErr.msg});
        });
    } else if (device !== null) {
      filterByDevice(device, data)
        .then(deviceResponse=>{
          resolve(deviceResponse);
        }, deviceErr=>{
          reject({code: deviceErr.code, msg: deviceErr.msg});
        });
    } else {
      resolve(data);
    }
  });
};

function filterByUser(userId, measurements){
  return new Promise((resolve, reject)=>{
    let userMeas = measurements.find(meas => meas.userId === userId);
    if (!userMeas.userId) {
      reject({code: 400, msg: 'No measurements found for requested user'});
    } else {
      resolve([userMeas]);
    }
  });
}

function filterByBuilding(buildingId, measurements){
  return new Promise((resolve, reject)=>{
    let buildingMeas = measurements.filter(meas => meas.building.buildingId == buildingId);
    if (buildingMeas.length < 1 || !buildingMeas || buildingMeas === undefined) {
      reject({code: 400, msg: 'No measurements found for requested building'});
    } else {
      resolve(buildingMeas);
    }
  });
}

function filterByVent(ventId, measurements){
  return new Promise((resolve, reject)=>{
    let returnVent = [];
    measurements.map(meas=>{
      meas.building.airVents.map(vent=>{
        if (vent.airVentId === ventId) {
          returnVent.push({
            userId: meas.userId,
            building: {
              buildingId: meas.building.buildingId,
              airVents: [vent]
            }
          });
        }
      });
    });
    if (returnVent.length < 1) {
      reject({code: 400, msg: 'No measurements found for requested airVent'});
    } else {
      resolve(returnVent);
    }
  });
}

function filterByFilter(filterId, measurements){
  return new Promise((resolve, reject)=>{
    let returnFilter = [];
    measurements.map(meas=>{
      meas.building.airVents.map(vent=>{
        vent.airFilters.map(filter=>{
          if (filter.airFilterId === filterId) {
            returnFilter.push({
              userId: meas.userId,
              building: {
                buildingId: meas.building.buildingId,
                airVents: {
                  airVentId: vent.airVentId,
                  airFilters: filter
                }
              }
            });
          }
        });
      });
    });
    if (returnFilter.length < 1) {
      reject({code: 400, msg: 'No measurements found for selected filter'});
    } else {
      resolve(returnFilter);
    }
  });
}

function filterByDevice(deviceId, measurements){
  return new Promise((resolve, reject)=>{
    let allDevices = [];
    measurements.map(meas=>{
      meas.building.airVents.map(vent=>{
        vent.airFilters.map(filter=>{
          if (filter.device == deviceId) {
            allDevices.push({
              userId: meas.userId,
              building: {
                buildingId: meas.building.buildingId,
                airVents: [{
                  airVentId: vent.airVentId,
                  airFilters: filter
                }]
              }
            });
          }
        });
      });
    });
    if (allDevices.length < 1 || !allDevices || allDevices === undefined) {
      reject({code: 400, msg: 'No measurements found for requested device'});
    } else {
      resolve(allDevices);
    }
  });
}
