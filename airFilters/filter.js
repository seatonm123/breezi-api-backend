exports.runFilters = function(user, building, vent, filter, data){
  return new Promise((resolve, reject)=>{
    if (user !== null) {
      runUser(user, data)
        .then(userResponse=>{
          resolve(userResponse);
        }, userErr=>{
          reject({code: userErr.code, msg: userErr.msg});
        });
    } else if (building !== null) {
      runBuilding(building, data)
        .then(buildingResponse=>{
          resolve(buildingResponse);
        }, buildingErr=>{
          reject({code: buildingErr.code, msg: buildingErr.msg});
        });
    } else if (vent !== null) {
      runVent(vent, data)
        .then(ventResponse=>{
          resolve(ventResponse);
        }, ventErr=>{
          reject({code: ventErr.code, msg: ventErr.msg});
        });
    } else if (filter !== null) {
      runFilter(filter, data)
        .then(filterResponse=>{
          resolve(filterResponse)
        }, filterErr=>{
          reject({code: filterErr.code, msg: filterErr.msg});
        });
    } else {
      formatAll(data)
        .then(dataResponse=>{
          resolve(dataResponse)
        }, dataErr=>{
          reject({code: dataErr.code, msg: dataErr.msg});
        });
    }
  });
};

function formatAll(buildings){
  return new Promise((resolve, reject)=>{
    let returnFilters = [];
    buildings.map(building=>{
      building.building.airVents.map(vent=>{
        vent.airFilters.map(filter=>{
          returnFilters.push({
            airFilterId: filter,
            userId: building.userId,
            buildingId: building.building.buildingId,
            airVent: vent.airVentId,
            device: vent.device
          });
        });
      });
    });
    if (returnFilters.length < 1) {
      reject({code: 400, msg: 'No airFilters affiliated with this partner found'});
    } else {
      resolve(returnFilters);
    }
  });
}

function runUser(userId, buildings){
  return new Promise((resolve, reject)=>{
    let returnBuildings = buildings.filter(building => building.userId === userId);
    if (returnBuildings.length < 1) {
      reject({code: 400, msg: 'No airFilters found for requested user'});
    } else {
      formatAll(returnBuildings)
        .then(formattedData=>{
          resolve(formattedData);
        }, formatErr=>{
          reject({code: formatErr.code, msg: formatErr.msg});
        });
    }
  });
}

function runBuilding(buildingId, buildings){
  return new Promise((resolve, reject)=>{
    let returnBuildings = buildings.filter(building => building.building.buildingId === buildingId);
    if (returnBuildings.length < 1) {
      reject({code: 400, msg: 'No airFilters found for requested building'});
    } else {
      formatAll(returnBuildings)
        .then(formattedData=>{
          resolve(formattedData);
        }, formatErr=>{
          reject({code: formatErr.code, msg: formatErr.msg});
        });
    }
  });
}

function runVent(ventId, buildings){
  return new Promise((resolve, reject)=>{
    let returnVent = [];
    buildings.map(building=>{
      building.building.airVents.map(vent=>{
        if (vent.airVentId === ventId) {
          vent.airFilters.map(filter=>{
            returnVent.push({
              airVentId: ventId,
              userId: building.userId,
              buildingId: building.building.buildingId,
              device: vent.device,
              airFilterId: filter
            });
          });
        }
      });
    });
    if (returnVent.length < 1) {
      reject({code: 400, msg: 'No airFilters found for requested airVent'});
    } else {
      resolve(returnVent);
    }
  });
}

function runFilter(filterId, buildings){
  return new Promise((resolve, reject)=>{
    let returnFilter = [];
    buildings.map(building=>{
      building.building.airVents.map(vent=>{
        vent.airFilters.map(filter=>{
          if (filter === filterId) {
            returnFilter.push({
              airFilterId: filter,
              userId: building.userId,
              buildingId: building.building.buildingId,
              airVent: vent.airVentId,
              device: vent.device
            });
          }
        });
      });
    });
    if (returnFilter.length < 1) {
      reject({code: 400, msg: 'No airFilter found with requested id'});
    } else if (returnFilter.length > 1) {
      reject({code: 400, msg: 'ERROR: more than one airFilter exists with requested id'});
    } else {
      resolve(returnFilter);
    }
  });
}
