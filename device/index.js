const Core = require('./core/core');
const Filter = require('./filter');

exports.handler = function(event, context, cb){
  const response = (code, msg)=>{
    if (msg) console.log(JSON.stringify(msg, null, 1));
    cb(null, {
      statusCode: code,
      body: msg ? JSON.stringify(msg) : '',
      headers: {
        "Content-Type": "application/json"
      }
    });
  };

  let accessToken = event.headers && event.headers.Authorization ?
    event.headers.Authorization : null;

  let userParam = event.queryStringParameters && event.queryStringParameters.user ?
    event.queryStringParameters.user : null;

  let buildingParam = event.queryStringParameters && event.queryStringParameters.building ?
    event.queryStringParameters.building : null;

  let ventParam = event.queryStringParameters && event.queryStringParameters.airVent ?
    event.queryStringParameters.airVent : null;

  let deviceParam = event.queryStringParameters && event.queryStringParameters.device ?
    event.queryStringParameters.device: null;

  if (event.queryStringParameters) {
    let counter = 0;
    for (param in event.queryStringParameters) {
      counter += 1;
    }
    if (counter > 1) {
      response(422, 'More than one id query parameter included in url string');
    }
  }

  Core.runCore(accessToken)
    .then(coreData=>{
      if (event.httpMethod === "GET") {
        Filter.runFilters(userParam, buildingParam, ventParam, deviceParam, coreData)
          .then(filteredDevices=>{
            response(200, filteredDevices);
          }, filterErr=>{
            response(filterErr.code, filterErr.msg);
          });
      } else {
        response(303, 'HTTP method not supported. Please see the documentation in the Help section of the API client for details on modeling an HTTP request');
      }
    }, coreErr=>{
      response(coreErr.code, coreErr.msg);
    });

};
