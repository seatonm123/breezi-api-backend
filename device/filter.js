exports.runFilters = function(user, building, vent, device, data){
  return new Promise((resolve, reject)=>{
    if (user !== null) {
      runUser(user, data)
        .then(userResponse=>{
          resolve(userResponse);
        }, userErr=>{
          reject({code: userErr.code, msg: userErr.msg});
        });
    } else if (building !== null) {
      runBuilding(building, data)
        .then(buildingResponse=>{
          resolve(buildingResponse);
        }, buildingErr=>{
          reject({code: buildingErr.code, msg: buildingErr.msg});
        });
    } else if (vent !== null) {
      runVent(vent, data)
        .then(ventResponse=>{
          resolve(ventResponse);
        }, ventErr=>{
          reject({code: ventErr.code, msg: ventErr.msg});
        });
    } else if (device !== null) {
      runDevice(device, data)
        .then(deviceResponse=>{
          resolve(deviceResponse);
        }, deviceErr=>{
          reject({code: deviceErr.code, msg: deviceErr.msg});
        });
    } else {
      formatAll(data)
        .then(formattedData=>{
          resolve(formattedData);
        }, formatErr=>{
          reject({code: formatErr.code, msg: formatErr.msg});
        });
    }
  });
};

function formatAll(buildings){
  return new Promise((resolve, reject)=>{
    let returnDevices = [];
    buildings.map(building=>{
      building.building.airVents.map(vent=>{
        returnDevices.push({
          device: vent.device,
          userId: building.userId,
          buildingId: building.building.buildingId,
          airVentId: vent.airVentId
        });
      });
    });
    if (returnDevices.length < 1) {
      reject({code: 400, msg: 'No devices found for this partner'});
    } else {
      resolve(returnDevices);
    }
  });
}

function runUser(userId, buildings){
  return new Promise((resolve, reject)=>{
    let returnUser = buildings.filter(building => building.userId === userId);
    if (returnUser.length < 1) {
      reject({code: 400, msg: 'No devices found for requested user'});
    } else {
      formatAll(returnUser)
        .then(formattedData=>{
          resolve(formattedData);
        }, formatErr=>{
          reject({code: formatErr.code, msg: formatErr.msg});
        });
    }
  });
}

function runBuilding(buildingId, buildings){
  return new Promise((resolve, reject)=>{
    let returnBuilding = buildings.filter(building => building.building.buildingId === buildingId);
    if (returnBuilding.length < 0) {
      reject({code: 400, msg: 'No devices found for requested building'});
    } else {
      formatAll(returnBuilding)
        .then(formattedData=>{
          resolve(formattedData);
        }, formatErr=>{
          reject({code: formatErr.code, msg: formatErr.msg});
        });
    }
  });
}

function runVent(ventId, buildings){
  return new Promise((resolve, reject)=>{
    let returnVent = [];
    buildings.map(building=>{
      building.building.airVents.map(vent=>{
        if (vent.airVentId === ventId) {
          returnVent.push({
            airVentId: vent.airVentId,
            device: vent.device,
            userId: building.userId,
            buildingId: building.building.buildingId
          });
        }
      });
    });
    if (returnVent.length < 1) {
      reject({code: 400, msg: 'No devices found for selected airVent'});
    } else {
      resolve(returnVent);
    }
  });
}

function runDevice(deviceId, buildings){
  return new Promise((resolve, reject)=>{
    let returnDevice = [];
    buildings.map(building=>{
      building.building.airVents.map(vent=>{
        if (vent.device == deviceId) {
          returnDevice.push({
            deviceId: deviceId,
            userId: building.userId,
            buildingId: building.building.buildingId,
            airVentId: vent.airVentId
          });
        }
      });
    });
    if (returnDevice.length < 1) {
      reject({code: 400, msg: 'No devices found with requested id'});
    } else if (returnDevice.length > 1) {
      reject({code: 400, msg: 'ERROR: More than one device exists with requested id'});
    } else {
      resolve(returnDevice);
    }
  });
}
