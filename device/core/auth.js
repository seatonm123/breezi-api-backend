const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});

const Cognito = new AWS.CognitoIdentityServiceProvider();

exports.verifyPartner = function(jwt){
  return new Promise((resolve, reject)=>{
    if (jwt === null) {
      reject({code: 403, msg: 'No Access Token present in Authorization header. See documentation in Breezi API client for information on API authorization'});
    } else {
      Cognito.getUser({AccessToken: jwt}, (err, partner)=>{
        if (err) {
          reject ({code: 401, msg: 'The given Access Token is invalid. Please ensure it is entered correctly and that it has not expired'});
        } else {
          resolve(partner);
        }
      });
    }
  });
};
