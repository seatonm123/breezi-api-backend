const Core = require('./core/core');
const DAP = require('./dap');
const Filter = require('./filter');
const Query = require('./query');

exports.handler = function(event, context, cb){
  const response = (code, msg)=>{
    if (msg) console.log(JSON.stringify(msg, null, 1));
    cb(null, {
      statusCode: code,
      body: msg ? JSON.stringify(msg) : '',
      headers: {
        "Content-Type": "application/json"
      }
    });
  };

  let accessToken = event.headers && event.headers.Authorization ?
    event.headers.Authorization : null;

  let userParam = event.queryStringParameters && event.queryStringParameters.user ?
    event.queryStringParameters.user : null;

  let buildingParam = event.queryStringParameters && event.queryStringParameters.building ?
    event.queryStringParameters.building : null;

  let ventParam = event.queryStringParameters && event.queryStringParameters.airVent ?
    event.queryStringParameters.airVent : null;

  let filterParam = event.queryStringParameters && event.queryStringParameters.airFilter ?
    event.queryStringParameters.airFilter : null;

  let deviceParam = event.queryStringParameters && event.queryStringParameters.device ?
    event.queryStringParameters.device : null;

  let fromParam = event.queryStringParameters && event.queryStringParameters.fromTs ?
    event.queryStringParameters.fromTs : null;

  let toParam = event.queryStringParameters && event.queryStringParameters.toTs ?
    event.queryStringParameters.toTs : null;

  let filterParams = [userParam, buildingParam, ventParam, filterParam, deviceParam];
  if (event.queryStringParameters) {
    let counter = 0;
    filterParams.map(param=>{
      if (param !== null) {
        counter += 1;
      }
    });
    if (counter > 1)  {
      response(422, 'More than one query parameter included in url string');
    }
  }

  Core.runCore(accessToken)
    .then(coreData=>{
      if (event.httpMethod === "GET") {
        DAP.getAllDaps(coreData)
          .then(allDaps=>{
            Filter.runFilters(userParam, buildingParam, ventParam, filterParam, deviceParam, allDaps)
              .then(filterDaps=>{
                Query.runQueries(fromParam, toParam, filterDaps)
                  .then(queriedDaps=>{
                    response(200, queriedDaps);
                  }, queryErr=>{
                    response(queryErr.code, queryErr.msg);
                  });
              }, filterErr=>{
                response(filterErr.code, filterErr.msg);
              });
          }, allErr=>{
            response(allErr.code, allErr.msg);
          });
      } else {
        response(303, 'HTTP method not supported. Please see the documentation in the Help section of the API client for details on modeling an HTTP request');
      }
    }, coreErr=>{
      response(coreErr.code, coreErr.msg);
    });

};
