const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});

const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

const DAP_TABLE = 'DataAnalysisPackets';
const ENVIRONMENT_SUFFIX = '-matt';

exports.getAllDaps = function(buildings){
  return new Promise((resolve, reject)=>{
    Dynamo.scan({TableName: DAP_TABLE + ENVIRONMENT_SUFFIX}, (err, daps)=>{
      if (err) {
        reject({code: 400, msg: 'No dataAnalysisPackets found affiliated with this partner'});
      } else {
        let promises = buildings.map(building => getDapsByBuilding(building, daps.Items));
        Promise.all(promises)
          .then(buildingDaps=>{
            resolve(buildingDaps);
          }, buildingErr=>{
            reject({code: buildingErr.code, msg: buildingErr.msg});
          });
      }
    });
  });
};

function getDapsByBuilding(building, daps){
  return new Promise((resolve, reject)=>{
    building.building.airVents.map(vent=>{
      let returnFilters = [];
      vent.airFilters.map(filter=>{
        let returnObj = {
          airFilterId: filter,
          dataAnalysisPackets: daps.filter(d => d.objectId === filter).sort((a, b)=>{return a.timestamp - b.timestamp})
        };
        returnFilters.push(returnObj);
      });
      vent.airFilters = returnFilters;
    });
    resolve(building);
  });
}
