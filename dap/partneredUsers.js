const AWS = require('aws-sdk');
AWS.config.update({region: 'eu-west-1'});

const doc = require('dynamodb-doc');
const Dynamo = new doc.DynamoDB();

const USERS_TABLE = 'Users';
const ENVIRONMENT_SUFFIX = '-matt';

exports.retrievePartneredUsers = function(partner){
  return new Promise((resolve, reject)=>{
    Dynamo.scan({TableName: USERS_TABLE + ENVIRONMENT_SUFFIX}, (err, users)=>{
      if (err) {
        reject({code: 400, msg: 'No users found'});
      } else {
        let partneredUsers = users.Items.filter(u => u.partnerId === partner.Username);
        if (partneredUsers.length > 0) {
          resolve(partneredUsers);
        } else {
          reject({code: 400, msg: 'Current partner has no affiliated users'});
        }
      }
    });
  });
};
