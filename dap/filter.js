exports.runFilters = function(user, building, vent, filter, device, data){
  return new Promise((resolve, reject)=>{
    if (user !== null) {
      runUser(user, data)
        .then(userDaps=>{
          resolve(userDaps);
        }, userErr=>{
          reject({code: userErr.code, msg: userErr.msg});
        });
    } else if (building !== null) {
      runBuilding(building, data)
        .then(buildingDaps=>{
          resolve(buildingDaps);
        }, buildingErr=>{
          reject({code: buildingErr.code, msg: buildingErr.msg});
        });
    } else if (vent !== null) {
      runVent(vent, data)
        .then(ventDaps=>{
          resolve(ventDaps);
        }, ventErr=>{
          reject({code: ventErr.code, msg: ventErr.msg});
        })
    } else if (filter !== null) {
      runFilter(filter, data)
        .then(filterDaps=>{
          resolve(filterDaps);
        }, filterErr=>{
          reject({code: filterErr.code, msg: filterErr.msg});
        });
    } else if (device !== null) {
      runDevice(device, data)
        .then(deviceDaps=>{
          resolve(deviceDaps);
        }, deviceErr=>{
          reject({code: deviceErr.code, msg: deviceErr.msg});
        });
    } else {
      resolve(data);
    }
  });
};

function runUser(userId, daps){
  return new Promise((resolve, reject)=>{
    let returnUser = daps.filter(dap => dap.userId === userId);
    if (returnUser.length < 1) {
      reject({code: 400, msg: 'No dataAnalysisPackets found for selected user'});
    } else {
      resolve(returnUser.filter(rU => rU.building.airVents.length > 0));
    }
  });
}

function runBuilding(buildingId, daps){
  return new Promise((resolve, reject)=>{
    let returnBuilding = daps.filter(dap => dap.building.buildingId === buildingId);
    if (returnBuilding.length < 1) {
      reject({code: 400, msg: 'No dataAnalysisPackets found for selected building'});
    } else {
      resolve(returnBuilding.filter(rB => rB.building.airVents.length > 0));
    }
  });
}

function runVent(ventId, daps){
  return new Promise((resolve, reject)=>{
    daps.map(dap=>{
      let returnVent = [];
      dap.building.airVents.map(vent=>{
        if (vent.airVentId === ventId) {
          returnVent.push(vent);
        }
      });
      dap.building.airVents = returnVent;
    });
    resolve(daps.filter(d => d.building.airVents.length > 0));
  });
}

function runFilter(filterId, daps){
  return new Promise((resolve, reject)=>{
    let returnFilter = {};
    daps.map(dap=>{
      dap.building.airVents.map(vent=>{
        vent.airFilters.map(filter=>{
          if (filter.airFilterId === filterId) {
            returnFilter = {
              userId: dap.userId,
              building: {
                buildingId: dap.building.buildingId,
                airVents: {
                  airVentId: vent.airVentId,
                  device: vent.device,
                  airFilters: filter
                }
              }
            };
          }
        });
      });
    });
    if (!returnFilter.userId) {
      reject({code: 400, msg: 'No dataAnalysisPackets found for requested airFilter'});
    } else {
      resolve([returnFilter]);
    }
  });
}

function runDevice(deviceId, daps){
  return new Promise((resolve, reject)=>{
    let returnDevice = {};
    daps.map(dap=>{
      dap.building.airVents.map(vent=>{
        if (vent.device === deviceId) {
          returnDevice = {
            userId: dap.userId,
            building: {
              buildingId: dap.building.buildingId,
              airVents: [vent]
            }
          };
        }
      });
    });
    if (!returnDevice.userId) {
      reject({code: 400, msg: 'No dataAnalysisPackets found for requested device'});
    } else {
      resolve([returnDevice]);
    }
  });
}
