exports.runQueries = function(from, to, data){
  return new Promise((resolve, reject)=>{
    runFrom(from, data)
      .then(fromDaps=>{
        runTo(to, fromDaps)
          .then(toDaps=>{
            resolve(toDaps);
          }, toErr=>{
            reject({code: toErr.code, msg: toErr.msg});
          });
      }, fromErr=>{
        reject({code: fromErr.code, msg: fromErr.msg});
      });
  });
};

function runFrom(from, daps){
  return new Promise((resolve, reject)=>{
    if (from === null) {
      resolve(daps);
    } else {
      let promises = daps.map(dap=>parseFrom(from, dap));
      Promise.all(promises)
        .then(fromDaps=>{
          resolve(fromDaps);
        }, fromErr=>{
          reject({code: 400, msg: 'No dataAnalysisPackets found after timestamp: ' + from.toString()});
        });
    }
  });
}

function parseFrom(from, dap){
  return new Promise((resolve, reject)=>{
    dap.building.airVents.map(vent=>{
      vent.airFilters.map(filter=>{
        filter.dataAnalysisPackets = filter.dataAnalysisPackets.filter(d => d.timestamp >= from);
      });
    });
    resolve(dap);
  });
}

function runTo(to, daps){
  return new Promise((resolve, reject)=>{
    if (to === null) {
      resolve(daps);
    } else {
      let promises = daps.map(dap=>parseTo(to, dap));
      Promise.all(promises)
        .then(toDaps=>{
          resolve(toDaps);
        }, fromErr=>{
          reject({code: 400, msg: 'No dataAnalysisPackets found before timestamp: ' + to.toString()});
        });
    }
  });
}

function parseTo(to, dap){
  return new Promise((resolve, reject)=>{
    dap.building.airVents.map(vent=>{
      vent.airFilters.map(filter=>{
        filter.dataAnalysisPackets = filter.dataAnalysisPackets.filter(d => d.timestamp <= to);
      });
    });
    resolve(dap);
  });
}
